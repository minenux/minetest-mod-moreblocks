--[[
=====================================================================
** More Blocks **
By Calinou, with the help of ShadowNinja and VanessaE.

Copyright © 2011-2019 Hugo Locurcio and contributors.
Licensed under the zlib license. See LICENSE.md for more information.
=====================================================================
--]]

moreblocks = {}
local is_54 = minetest.has_feature("direct_velocity_on_players")
local is_50 = minetest.has_feature("object_use_texture_alpha")
local modpath = minetest.get_modpath("moreblocks")
local S, NS = dofile(modpath .. "/intllib.lua")
moreblocks.S = S
moreblocks.NS = NS
moreblocks.is_50 = is_50
moreblocks.is_54 = is_54
dofile(modpath .. "/config.lua")
dofile(modpath .. "/circular_saw.lua")
dofile(modpath .. "/stairsplus/init.lua")
dofile(modpath .. "/nodes.lua")
dofile(modpath .. "/redefinitions.lua")
dofile(modpath .. "/crafting.lua")
dofile(modpath .. "/aliases.lua")
