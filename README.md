minetest mod More Blocks
========================

More Blocks for minetest.

Information
--------------

This mod adds a lot of new blocks, most of them don't have a use other than decoration.

Adds a lot of useful shapes (diffrent stairs, slabs, panels, ..) 
and some decorative blocks which carry building to a new level.

![screenshot.png](screenshot.png)

Larger image at [screenshot_large.png](screenshot_large.png)

### New blocks and items:

- Iron stone brick. Brighter than regular stone brick.

Crafting (X = iron ingot, O = stone brick) = 1:
```
X
O
```
- Plank stone. Can be used for nice floors.

Crafting (X = plank, O = smooth stone) = 4:
```
X O
O X
or
O X
X O
```

- Stone tiles. They kind of look like Minecraft double smooth stone slabs, especially the block 43-6. Can be used to craft stairs, slabs and panels like in the Stairs+ mod.

Crafting (X = cobblestone) = 4:
```
X X
X X
```

- Cactus brick. Basically, green brick.

Crafting (X = cactus, O = brick block) = 1:
```
X
O
```

- Cactus checker. Can be used for nice floors.

Crafting (X = cactus, O = smooth stone) = 4:
```
X O
O X
or
O X
X O
```

- Coal checker. Can be used for nice floors.

Crafting (X = coal lump, O = smooth stone) = 4:
```
X O
O X
or
O X
X O
```

- Coal glass, darker glass.

Crafting (X = coal lump, O = glass) = 1:
```
X
O
```

- Coal stone, darker stone.

Crafting (X = coal lump, O = smooth stone) = 1:
```
X
O
```

- Empty bookshelf.

Crafting (X = sweeper [see below], O = bookshelf) = 1
```
X
O
```

- Glowing glass. Emits a moderate amount of light (less light than a torch, a bit more than sunrise light).

Crafting (X = torch, O = glass) = 1
```
X
O
```

- Super glowing glass. Emits a high amount of light (a bit more light than a torch).
Crafting (X = torch, O = glass)
```
X
X
O
```
Produces 1 super glowglass.

- Clean glass.
Crafting (X = sweeper, O = glass)
```
X
O
```
Produces 1 clean glass.

- Iron checker. Can be used for nice floors.
Crafting (X = iron ingot, O = smooth stone)
```
X O
O X
```
or
```
O X
X O
```
Produces 4 iron checkers.

- Iron glass, gray, lighter glass.
Crafting (X = iron ingot, O = glass)
```
X
O
```
Produces 1 iron glass.

- Wooden tiles.
Crafting (X = wooden plank)
```
X X X
X X X
X X X
```
Produces 9 wooden tiles.

- Centered wooden tiles.
Crafting (X = wooden plank, O = wooden tile)
```
X X X
X O X
X X X
```

- Full wooden tiles.
Crafting (O = wooden tile)
```
O O
O O
```

- Directional wooden tiles.
Crafting (O = centered wooden tile, X = stick)
```
O
X
```
(for downwards version)

- Iron stone, brighter stone.
Crafting (X = iron ingot, O = smooth stone)
```
X
O
```
Produces 1 iron stone.

- Trap Stone. Stone that you can pass through. When you are inside, the screen turns black, this is intended.
Crafting (X = MESE, O = smooth stone):
```
X
O
```
Produces 12 trapstones.

- Trap Glass. Glass that you can pass through. Unlike trapstone, the screen doesn't turn black when you are inside.
Crafting (X = MESE, O = glass):
```
X
O
```
Produces 12 trapglasses.


- Rope, climbable like ladders.
Three crafting recipes possible, they all work (X = leaves, O = jungle grass):
```
X
X
X
```
or
```
X
O
X
```
or
```
O
O
O
```
Produces 1 climbable rope. Ropes do not spread/grow.

- Sweepers, an item used in crafting of empty bookshelves and clean glass.
Crafting (X = jungle grass, O = stick)
```
X
O
```
Produces 3 sweepers.

### New recipes, but not really new blocks

- Sticks:
Can be crafted from dry shrubs, just put one dry shrub in the crafting grid.

- Dirt with Grass:
Can be crafted by putting 1 Mese or a jungle grass over a dirt block in the crafting grid.

- Iron ingots:
Iron blocks can be crafted back to 9 iron ingots.
Simply put an iron block in the crafting grid.

- Sapling:
Saplings can be crafted with 6 leaf blocks and 1 stick.
Crafting (X = leaves, O = stick, _ = nothing):
```
X X X
X X X
_ O _
```

- Locked Chest:
Chests may be "upgraded" to locked chests.
Crafting (X = iron ingot, O = chest):
```
X
O
```

### Stairs+ recipes

Stairs+ adds:

- "panels", aka "quarter blocks" - can be placed on floor, wall or ceiling.

- corner stairs (inner and outer)

- "microblocks": small (1/8th) blocks

- "quarter slabs"

- four new slab/stair materials: steel block, desert stone, mossy cobblestone, glass, obsidian, obsidian glass

- most More Blocks materials can also be used

- sounds for slabs/stairs

- recipe improvements (more logical)

- upside-down slabs

- "alternative" stairs

- slabs placeable on walls

- stairs placeable on walls

- "half" variation of wall stairs

- upside-down stairs

- light passes through stairs/slabs/panels/microblocks (their textures are no longer darkened)

Crafting recipes:
- Circular Saw, to craft stairs/slabs/panels/microblocks:
```
_ X _
W W W
W _ W
X = steel ingot, W = wood.
```

There are several ways to flip Stairs+ nodes:
- using a screwdriver
- placing them on a wall or ceiling

Technical information
-----------------------

[**Forum topic**](https://forum.minetest.net/viewtopic.php?f=11&t=509)

### Installation

Clone this Git repository into your Minetest's `mods/` directory 
as `git clone https://codeberg.org/minenux/minetest-mod-moreblocks moreblocks` to
your mods directory, original mod its only 5.4+ compatible from and 
as `git clone https://github.com/minetest-mods/moreblocks.git` .

the resulting directory must be renamed to `moreblocks` – this is
**absolutely** required, as the mod won't work otherwise.

1. Make sure Minetest is not currently running (otherwise, it will overwrite
   the changes when exiting).
2. Open the world's `world.mt` file using a text editor.
3. Add or get sure of the following line at the file: `load_mod_moreblocks = true`
4. Save the file, then start a game on the world you enabled More Blocks on.
5. More Blocks should now be running on your world.

## Version compatibility

This frork is from https://codeberg.org/minenux/minetest-mod-moreblocks on brach `main`
Its aims to be compatible with 0.4.16+ to 5.2, and it seems compatible with 5.4+ too
It may or may not work with newer or older versions. Issues arising in older
versions than 5.4 will generally not be fixed, here we try to. but always with a PR.

## License

Copyright © 2011-2019 Hugo Locurcio and contributors

- More Blocks code is licensed under the zlib license, see
  [`LICENSE.md`](LICENSE.md) for details.
- Unless otherwise specified, More Blocks textures are licensed under
  [CC BY-SA 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/).

`moreblocks_copperpatina.png` was created by pithydon, and is licensed under
[CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/).
